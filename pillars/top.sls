base:
  'env:prd':
    - match: grain
    - prd
    - logstash

  'env:dev':
    - match: grain
    - dev
    - logstash

  'role:elasticsearch':
    - match: grain
    - elasticsearch

  'role:kibana':
    - match: grain
    - kibana

  'role:xldeploy':
    - match: grain
    - xldeploy
