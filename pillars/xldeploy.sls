xldeploy-server:
  source: salt://xldeploy/files/xl-deploy-4.0.1-server.zip
  source-hash: md5=fed2ba78c6c1b3fd8588be9197df3c7d
  version: 4.0.1
  base-location: /opt/xl-deploy
  repository-location: /opt/xl-deploy/repository
  license-location: salt://xldeploy/files/conf/deployit-license.lic
  plugins:
    windows-plugin-4.0.0:
      source: salt://xldeploy/files/plugins/windows-plugin-4.0.0.jar
      source_hash: md5=1f0f40304032fe02f8128c6f52c5443f
    command2-plugin-4.0.0:
      source: salt://xldeploy/files/plugins/command2-plugin-4.0.0.jar
      source_hash: md5=68486f91be335e68c634b9fc0190b099
  hotfix:
    logstash-logback-encoder-2.9:
      source: salt://xldeploy/files/hotfix/logstash-logback-encoder-2.9.jar
      source_hash: md5=8a1cc44580f995a59c6a77848d489665
  conf:
    deployit.conf:
      source: salt://xldeploy/files/conf/deployit.conf.jinja
    logback.xml:
      source: salt://xldeploy/files/conf/logback.xml.jinja

xldeploy-cli:
  source: salt://xldeploy/files/xl-deploy-4.0.1-cli.zip
  source-hash: md5=19bad824ae0fecf103c45d58b8fc1af4
  version: 4.0.1
  base-location: /opt/xl-deploy
