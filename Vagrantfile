master = {
  hostname: 'salt.localdomain',
  ip: '192.168.168.10',
  memory: '512'
}

minions = {
  kibana: {
    hostname: 'kibana.localdomain',
    ip: '192.168.168.100',
    memory: 512,
    ports: [
      { guest: 80, host: 8080 }
    ],
    role: 'kibana',
    env: 'dev'
  },
  es1: {
    hostname: 'es1.localdomain',
    ip: '192.168.168.101',
    memory: 512,
    ports: [
      { guest: 9200, host: 9200 }
    ],
    role: 'elasticsearch',
    env: 'dev'
  },
  es2: {
    hostname: 'es2.localdomain',
    ip: '192.168.168.102',
    memory: 512,
    role: 'elasticsearch',
    env: 'dev'
  },
  es3: {
    hostname: 'es3.localdomain',
    ip: '192.168.168.103',
    memory: 512,
    role: 'elasticsearch',
    env: 'dev'
  },
  xldeploy: {
    hostname: 'xldeploy.localdomain',
    ip: '192.168.168.104',
    memory: 512,
    ports: [
      { guest: 4516, host: 4516 }
    ],
    role: 'xldeploy',
    env: 'dev'
  }
}

VAGRANTFILE_API_VERSION = '2'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box_url = 'https://dl.dropbox.com/s/jcx5scu4h2lzyu5/centos-65-x64-base.box'
  config.vm.box = 'centos-65-x64-base'

  config.vm.synced_folder 'bootstrap', '/tmp/salt/bootstrap'

  config.vm.define 'master' do |master|
      master.vm.hostname = 'salt.localdomain'
      master.vm.network 'private_network', ip: '192.168.168.10'

      master.vm.synced_folder 'states', '/srv/salt/states'
      master.vm.synced_folder 'pillars', '/srv/salt/pillars'
      master.vm.synced_folder 'reactor', '/srv/salt/reactor'
      master.vm.synced_folder 'conf/master.d', '/etc/salt/master.d'

      master.vm.provider :virtualbox do |vb|
        vb.gui = true
        vb.customize ['modifyvm', :id, '--memory', '512']
      end

      minions.each do |k, v|
        master.vm.provision 'shell', inline: "echo #{v[:ip]} #{k} #{v[:hostname]} >> /etc/hosts"
      end

      master.vm.provision 'shell', inline: 'sh /tmp/salt/bootstrap/boot_master.sh'
      master.vm.provision 'shell', inline: 'sh /tmp/salt/bootstrap/boot_minion.sh'
      master.vm.provision 'shell', inline: "echo 'role: master' >> /etc/salt/grains"
      master.vm.provision 'shell', inline: "echo 'env: prd' >> /etc/salt/grains"
  end

  minions.each do |host, attributes|
    config.vm.define host do |h|
      h.vm.hostname = attributes[:hostname]
      h.vm.network 'private_network', ip: attributes[:ip]

      if attributes[:ports]
        attributes[:ports].each do |mapping|
          config.vm.network 'forwarded_port', guest: mapping[:guest], host: mapping[:host]
        end
      end

      h.vm.provider :virtualbox do |vb|
        vb.gui = true
        vb.customize ['modifyvm', :id, '--memory', attributes[:memory]]
      end

      minions.each do |k, v|
        h.vm.provision 'shell', inline: "echo #{v[:ip]} #{k} #{v[:hostname]} >> /etc/hosts" unless k == host
      end

      h.vm.provision 'shell', inline: 'echo "192.168.168.10 salt salt.localdomain" >> /etc/hosts'

      h.vm.provision 'shell', inline: 'sh /tmp/salt/bootstrap/boot_minion.sh'

      h.vm.provision 'shell', inline: "echo 'role: #{attributes[:role]}' >> /etc/salt/grains"
      h.vm.provision 'shell', inline: "echo 'env: #{attributes[:env]}' >> /etc/salt/grains"
    end
  end

end
