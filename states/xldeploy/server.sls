xldeploy-server-group:
  group.present:
    - name: xldeploy
    - system: True

xldeploy-server-user:
  user.present:
    - name: xldeploy
    - fullname: XL Deploy Runtime User
    - shell: /bin/nologin
    - home: {{ pillar['xldeploy-server']['base-location'] }}
    - system: True
    - gid_from_name: True

xldeploy-server-extract-archive:
  archive.extracted:
    - source: {{ pillar['xldeploy-server']['source'] }}
    - archive_format: zip
    - name: {{ pillar['xldeploy-server']['base-location'] }}
    - source_hash: {{ pillar['xldeploy-server']['source-hash'] }}
    - if_missing: {{ pillar['xldeploy-server']['base-location'] }}/xl-deploy-{{ pillar['xldeploy-server']['version'] }}-server

xldeploy-server-chown:
  file.directory:
    - name: {{ pillar['xldeploy-server']['base-location'] }}
    - user: xldeploy
    - group: xldeploy
    - recurse:
      - user
      - group

xldeploy-server-create-symlink:
  file.symlink:
    - name: {{ pillar['xldeploy-server']['base-location'] }}/xl-deploy-server
    - target: {{ pillar['xldeploy-server']['base-location'] }}/xl-deploy-{{ pillar['xldeploy-server']['version'] }}-server
    - user: xldeploy
    - group: xldeploy

{% for key, value in pillar['xldeploy-server']['plugins'].iteritems() %}
xldeploy-server-install-plugin-{{ key }}:
  file.managed:
    - name: {{ pillar['xldeploy-server']['base-location'] }}/xl-deploy-server/plugins/{{ key }}.jar
    - source: {{ value['source'] }}
    - source_hash: {{ value['source_hash'] }}
    - makedirs: True
    - user: xldeploy
    - group: xldeploy
{% endfor %}

{% for key, value in pillar['xldeploy-server']['hotfix'].iteritems() %}
xldeploy-server-install-hotfix-{{ key }}:
  file.managed:
    - name: {{ pillar['xldeploy-server']['base-location'] }}/xl-deploy-server/hotfix/{{ key }}.jar
    - source: {{ value['source'] }}
    - source_hash: {{ value['source_hash'] }}
    - makedirs: True
    - user: xldeploy
    - group: xldeploy
{% endfor %}

{% for key, value in pillar['xldeploy-server']['conf'].iteritems() %}
xldeploy-server-install-conf-{{ key }}:
  file.managed:
    - name: {{ pillar['xldeploy-server']['base-location'] }}/xl-deploy-server/conf/{{ key }}
    - source: {{ value['source'] }}
    - makedirs: True
    - template: jinja
    - user: xldeploy
    - group: xldeploy
{% endfor %}

xldeploy-server-copy-license:
  file.managed:
    - name: {{ pillar['xldeploy-server']['base-location'] }}/xl-deploy-server/conf/deployit-license.lic
    - source: {{ pillar['xldeploy-server']['license-location'] }}
    - user: xldeploy
    - group: xldeploy

xldeploy-server-initialize:
  cmd.run:
    - name: {{ pillar['xldeploy-server']['base-location'] }}/xl-deploy-server/bin/server.sh -setup -reinitialize -force -setup-defaults {{ pillar['xldeploy-server']['base-location'] }}/xl-deploy-server/conf/deployit.conf
    - user: xldeploy
    - group: xldeploy
    - unless: test -d {{ pillar['xldeploy-server']['repository-location'] }}

xldeploy-server-service-upstart:
  file.managed:
    - name: /etc/init/xldeploy.conf
    - source: salt://xldeploy/files/xldeploy.upstart.jinja
    - mode: 644
    - template: jinja

xldeploy-server-service:
  service.running:
    - name: xldeploy
    - watch:
      - file: {{ pillar['xldeploy-server']['base-location'] }}/xl-deploy-server/conf/deployit.conf
