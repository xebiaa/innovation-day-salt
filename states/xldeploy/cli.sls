xldeploy-cli-group:
  group.present:
    - name: xldeploy
    - system: True

xldeploy-cli-user:
  user.present:
    - name: xldeploy
    - fullname: XL Deploy Runtime User
    - shell: /bin/nologin
    - home: {{ pillar['xldeploy-cli']['base-location'] }}
    - system: True
    - gid_from_name: True

xldeploy-cli-extract-archive:
  archive.extracted:
    - source: {{ pillar['xldeploy-cli']['source'] }}
    - archive_format: zip
    - name: {{ pillar['xldeploy-cli']['base-location'] }}
    - source_hash: {{ pillar['xldeploy-cli']['source-hash'] }}
    - if_missing: {{ pillar['xldeploy-cli']['base-location'] }}/xl-deploy-{{ pillar['xldeploy-cli']['version'] }}-cli

xldeploy-cli-chown:
  file.directory:
    - name: {{ pillar['xldeploy-cli']['base-location'] }}
    - user: xldeploy
    - group: xldeploy
    - recurse:
      - user
      - group

xldeploy-cli-create-symlink:
  file.symlink:
    - name: {{ pillar['xldeploy-cli']['base-location'] }}/xl-deploy-cli
    - target: {{ pillar['xldeploy-cli']['base-location'] }}/xl-deploy-{{ pillar['xldeploy-cli']['version'] }}-cli
    - user: xldeploy
    - group: xldeploy
