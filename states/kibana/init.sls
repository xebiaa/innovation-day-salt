{% set source=pillar['kibana']['source'] %}
{% set dirname=source.split('/')[-1].strip('.zip') %}

kibana_nginx_install:
  pkg.installed:
    - name: nginx

kibana_install:
  archive.extracted:
    - name: /opt/kibana
    - source: {{ source }}
    - source_hash: {{ pillar['kibana']['source_hash'] }}
    - archive_format: zip
    - if_missing: /opt/kibana/{{ dirname }}/

kibana_config:
  file.managed:
    - name: /opt/kibana/{{ dirname }}/config.js
    - source: salt://kibana/files/config.js.jinja
    - template: jinja
    - context:
      es_url: {{ pillar['kibana']['elasticsearch_url'] }}

kibana_nginx_host:
  file.managed:
    - name: /etc/nginx/conf.d/default.conf
    - source: salt://kibana/files/nginx/default.conf.jinja
    - template: jinja
    - context:
      root: /opt/kibana/{{ dirname }}

kibana_nginx_service:
  service.running:
    - name: nginx
    - enable: True
