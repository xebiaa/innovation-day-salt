{% set source=pillar['elasticsearch']['source'] %}
{% set dirname=source.split('/')[-1].strip('.zip') %}

elasticsearch_install:
  archive.extracted:
    - source: {{ source }}
    - source_hash: {{ pillar['elasticsearch']['source_hash'] }}
    - name: /opt/elasticsearch
    - archive_format: zip
    - if_missing: /opt/elasticsearch/{{ dirname }}

elasticsearch_group:
  group.present:
    - name: elasticsearch
    - system: True

elasticsearch_user:
  user.present:
    - name: elasticsearch
    - fullname: Elasticsearch Runtime User
    - shell: /bin/nologin
    - home: /opt/elasticsearch/{{ dirname }}
    - system: True
    - gid_from_name: True

elasticsearch_chown:
  file.directory:
    - name: /opt/elasticsearch/{{ dirname }}
    - user: elasticsearch
    - group: elasticsearch
    - recurse:
      - user
      - group

elasticsearch_config:
  file.managed:
    - name: /opt/elasticsearch/{{ dirname }}/config/elasticsearch.yml
    - source: salt://elasticsearch/files/elasticsearch.yml.jinja
    - mode: 644
    - template: jinja

elasticsearch_upstart:
  file.managed:
    - name: /etc/init/elasticsearch.conf
    - source: salt://elasticsearch/files/elasticsearch.upstart.conf.jinja
    - mode: 644
    - template: jinja
    - context:
      dirname: {{ dirname }}

elasticsearch_service:
  service.running:
    - name: elasticsearch
    - enable: True
