common_pkgs:
  pkg.installed:
    - pkgs:
      - unzip
      - collectd

common_stop_iptables:
  service.dead:
    - name: iptables

