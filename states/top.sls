base:
  '*':
    - common
    - java.openjdk.7
    - logstash

  'role:kibana':
    - match: grain
    - kibana

  'role:elasticsearch':
    - match: grain
    - java.openjdk.7
    - elasticsearch

  'role:xldeploy':
    - match: grain
    - java.openjdk.7
    - xldeploy
