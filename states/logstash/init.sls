{% set source=pillar['logstash']['source'] %}
{% set dirname=source.split('/')[-1].strip('.zip') %}
{% set sincedb=pillar['logstash']['sincedb'] %}

logstash_install:
  archive.extracted:
    - name: /opt/logstash
    - source: {{ source }}
    - source_hash: {{ pillar['logstash']['source_hash'] }}
    - archive_format: zip
    - if_missing: /opt/logstash/{{ dirname }}

logstash_group:
  group.present:
    - name: logstash
    - system: True

logstash_user:
  user.present:
    - name: logstash
    - fullname: Logstash Runtime User
    - shell: /bin/nologin
    - home: /opt/logstash/{{ dirname }}
    - system: True
    - gid_from_name: True

logstash_chown:
  file.directory:
    - name: /opt/logstash/{{ dirname }}
    - user: logstash
    - group: logstash
    - recurse:
      - user
      - group

logstash_sincedb:
  file.directory:
    - name: /opt/logstash/{{ sincedb }}
    - user: logstash
    - group: logstash

logstash_config:
  file.managed:
    - name: /opt/logstash/{{ dirname }}/logstash.conf
    - source: salt://logstash/files/logstash.conf
    - mode: 644
    - template: jinja
    - context:
      es_host: {{ pillar['elasticsearch']['host'] }}
      sincedb: {{ sincedb }}

logstash_upstart_config:
  file.managed:
    - name: /etc/init/logstash.conf
    - source: salt://logstash/files/logstash.upstart.conf.jinja
    - template: jinja
    - context:
      dirname: {{ dirname }}

logstash_service:
  service.running:
    - name: logstash
    - enable: True
