#!/bin/sh

#
# Bootstrap script for a Salt minion node
#

# Install the epel testing repo ...
rpm -Uvh http://ftp.linux.ncsu.edu/pub/epel/6/i386/epel-release-6-8.noarch.rpm

# Install the salt minion process on this node as well ...
yum install -y salt-minion --enablerepo=epel-testing
chkconfig salt-minion on

# Start the salt minion process ...
service salt-minion start

exit 0
