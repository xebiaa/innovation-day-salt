#!/bin/sh

#
# Bootstrap script for a Salt master node
#

# Install the epel testing repo ...
rpm -Uvh http://ftp.linux.ncsu.edu/pub/epel/6/i386/epel-release-6-8.noarch.rpm

# Install the salt master ...
yum install -y salt-master --enablerepo=epel-testing
chkconfig salt-master on

# Start the salt master process ...
service salt-master start

exit 0
